//
//  OrderDetailsViewController.swift
//  YaEsta Business
//
//  Created by Arturo Escamilla on 30/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
import SVProgressHUD

class ProductTableViewCell: UITableViewCell{
    
    @IBOutlet weak var img_product: UIImageView!
    
    @IBOutlet weak var lbl_name_product: UILabel!
    
    @IBOutlet weak var lbl_description_product: UILabel!
}

class OrderDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    

    @IBOutlet weak var lbl_title: UILabel!
    var order = NSDictionary()
    var item_set = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let order_id = order["id"] as! Int
        
        lbl_title.text = "Orden #" + String(order_id)
        
        item_set = order["item_set"] as! NSArray
        //products = item_set["product"] as! NSArray
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item_set.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "product_cell", for: indexPath) as! ProductTableViewCell
        
        let itemDict  = item_set[indexPath.row] as! NSDictionary
        
        
        let productDict: NSDictionary = itemDict["product"] as! NSDictionary
        
        cell.lbl_name_product.text = productDict["name"] as? String
      
        cell.lbl_description_product.text = itemDict["observations"] as? String
        
        
        let shop_images = (productDict["picture_set"]! as! NSArray).mutableCopy() as! NSMutableArray
        if shop_images.count > 0
        {
            let shop_image_dic = shop_images[0] as! [String : AnyObject]
            let shop_image_url = shop_image_dic["picture"] as? String
            let imageUrl:URL = URL(string: shop_image_url!)!
            
            
            DispatchQueue.global(qos: .userInitiated).async {
                
                let imageData:NSData = NSData(contentsOf: imageUrl)!
                
                // When from background thread, UI needs to be updated on main_queue
                DispatchQueue.main.async {
                    let image = UIImage(data: imageData as Data)
                    cell.img_product.image = image
                    //self.img_shop.contentMode = UIViewContentMode.scaleAspectFit
                    
                }
            }
        }
    
        
        return cell
    }
    @IBAction func return_action(_ sender: Any) {
        
        
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func accept_action(_ sender: Any) {
        updateEstatus(estatus: 1)
    }
 
    @IBAction func ready_action(_ sender: Any) {
       updateEstatus(estatus: 3)
    }
    @IBAction func cancel_action(_ sender: Any) {
      updateEstatus(estatus: 2)
    }
    func sendNotification(status:String){
        let costumer = order["customer"] as! NSDictionary
        SVProgressHUD.show()
        let invoke = InvokeService()
        let dict = NSMutableDictionary()
        dict["token"] = costumer["token"] as! String
        dict["estatus"] = status
        dict["id_order"] = order["id"] as! Int
        
        invoke.sendNotification(dict) { (response, error) in
            if(response != nil)
            {
                
            }
            SVProgressHUD.dismiss(completion: {
            self.navigationController?.popViewController(animated: true)
            
            self.dismiss(animated: true, completion: nil)
            })
        }
    }
    func updateEstatus(estatus:Int)  {
        var status = ""
        switch estatus {
        case 1:
            status = "accepted"
            break
        case 2:
            status = "cancelledbybusiness"
            break
        case 3:
            status = "ready"
            break
        default:
            break
        }
        SVProgressHUD.show()
        let invoke2 = InvokeService()
        let dict = NSMutableDictionary()
        dict["status"] = status
        dict["order"] = order["id"] as! Int
        invoke2.updateOrder(dict) { (response, error) in
            if error != nil
            {
                return
            }
            if (response?.count)! > 0
            {
                SVProgressHUD.dismiss(completion: {
                        switch estatus {
                            case 1:
                                    self.sendNotification(status: "accept")
                            break
                            case 2:
                                    self.sendNotification(status: "cancel")
                            break
                            case 3:
                                    self.sendNotification(status: "ready")
                            break
                            default:
                            break
            }
            })
            }
            
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
