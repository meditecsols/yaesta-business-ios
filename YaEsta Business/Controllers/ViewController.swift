//
//  ViewController.swift
//  YaEsta Business
//
//  Created by Arturo Escamilla on 30/08/18.
//  Copyright © 2018 Meditec. All rights reserved.
//

import UIKit
import SVProgressHUD



class OrderTableViewCell: UITableViewCell{
    
    @IBOutlet weak var lbl_customer: UILabel!
    @IBOutlet weak var lbl_num_order: UILabel!
    
    @IBOutlet weak var lbl_estatus: UILabel!
    
}


class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var orders : NSMutableArray = []
    private let refreshControl = UIRefreshControl()
    @IBOutlet weak var tv_orders: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tv_orders.refreshControl = refreshControl
        } else {
            tv_orders.addSubview(refreshControl)
        }
      refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
    }
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Weather Data
        getOrder()
    }
    @objc func methodOfReceivedNotification(notification: Notification){
        getOrder()
         SharedFunctions .sharedInstance().showAlert(withMessage: "Tiene una nueva orden.")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getOrder()
    }
    func getOrder(){
        SVProgressHUD.show()
        let invoke = InvokeService()
        invoke.getOrder ({ (response, error) in
            if error != nil {
                SVProgressHUD.dismiss()
                self.refreshControl.endRefreshing()
                return
            }
            if (response?.count)! > 0
            {
                
                SVProgressHUD.dismiss(completion: {
                
                self.orders = response!
                
                DispatchQueue.main.async {
                    self.tv_orders.reloadData()
                    self.refreshControl.endRefreshing()
                    self.tv_orders.isHidden = false
                }
                
                })
            }
            else
            {
                 SVProgressHUD.dismiss(completion: {
                self.tv_orders.isHidden = true
                })
            }
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "order_cell", for: indexPath) as! OrderTableViewCell
        let ordersDict  = orders[indexPath.row] as! NSDictionary
        
        let id = ordersDict["id"] as! Int
        cell.lbl_num_order.text = "#" + String(id)
        
        let customer = ordersDict["customer"] as! NSDictionary
        cell.lbl_customer.text = (customer["first_name"] as! String)
        let status = ordersDict["order_status"] as? String
        var status_name = ""
        if status == "open"
        {
            status_name = "Nueva orden"
            cell.lbl_estatus.textColor = UIColor.darkGray
        }
        else if status == "ready"
        {
            cell.lbl_estatus.textColor = UIColor.gray
            status_name = "Lista"
        }
        else if status == "cancelledbybusiness"
        {
            cell.lbl_estatus.textColor = UIColor.red
            status_name = "Cancelada"
        }
        else if status == "accepted"
        {
            cell.lbl_estatus.textColor = UIColor.orange
            status_name = "En progreso"
        }
        cell.lbl_estatus.text = status_name
        
        
        return cell
     }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "details_segue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "details_segue" {
            if let indexPath = self.tv_orders.indexPathForSelectedRow{
                let selectedRow = indexPath.row
                let orderDict  = orders[selectedRow] as! NSDictionary
                tv_orders.deselectRow(at: indexPath, animated: true)
                let od : OrderDetailsViewController = segue.destination as! OrderDetailsViewController
                od.order = orderDict
                
            }
            
        }
     
    }

}

