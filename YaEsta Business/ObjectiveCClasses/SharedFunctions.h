//
//  SharedFunctions.h

//
//  Created by Martin Gonzalez on 03/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



@interface SharedFunctions : NSObject
+(SharedFunctions *) sharedInstance;
-(UIViewController*)topMostController;
-(NSString *)formatDateToWS:(NSDate *)date ;
-(NSString *)getCurrentStoryboard;
-(NSArray *)arrayByReplacingNullsWithBlanks  : (NSArray *) array;
-(NSMutableDictionary *) dictionaryByReplacingNullsWithStrings : (NSMutableDictionary *) dictionary;
-(NSString *)getDateFormatToWS;

-(void)showAlertWithMessage:(NSString *)message;

@end
