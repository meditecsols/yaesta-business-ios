//
//  InvokeService.m

//
//  Created by Martin Gonzalez on 27/09/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "InvokeService.h"
#import "Config.h"
@import MobileCoreServices;
@implementation InvokeService


#pragma mark GetOrder
- (void)getOrder:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    
    NSString *authString=@"Token 177c7dd2768d68dec3cf48ecf70ac8eeae131b7b";
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/orderinfo/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}

#pragma mark UpdateOrder
- (void)updateOrder:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    
    NSString *authString=[NSString stringWithFormat:@"Token 177c7dd2768d68dec3cf48ecf70ac8eeae131b7b"];
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/orderstatus/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
 
}

#pragma mark Send Notification
- (void)sendNotification:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSString * _Nullable responseData, NSError * _Nullable error))completion{
    
    
    //NSString *authString=[NSString stringWithFormat:@"Token 177c7dd2768d68dec3cf48ecf70ac8eeae131b7b"];
    
    //NSDictionary *headers = @{ @"content-type": @"application/json",
                               //@"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"http://pamliljehult.inntecmovil.com/notificationCliente.php?token=%@&estatus=%@&id_order=%@",[dictInfo objectForKey:@"token"],[dictInfo objectForKey:@"estatus"],[dictInfo objectForKey:@"id_order"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    //[request setAllHTTPHeaderFields:headers];
    //[request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    NSString *ws_responce_string;
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        ws_responce_string = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce_string,error);
                                                }];
    [dataTask resume];
    
}

#pragma mark -
- (NSString *)mimeTypeForPath:(NSString *)path {
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}
- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}
- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                             paths:(NSArray *)paths
                         fieldNames:(NSArray *)fieldNames {
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    int i = 0;
    for (NSString *path in paths) {
        NSString *filename  = [path lastPathComponent];
        NSData   *data      = [NSData dataWithContentsOfFile:path];
        NSString *mimetype  = [self mimeTypeForPath:path];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [fieldNames objectAtIndex:i], filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        i++;
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

@end
