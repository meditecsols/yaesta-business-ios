//
//  InvokeService.h

//
//  Created by Martin Gonzalez on 27/09/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface InvokeService : NSObject

- (void)updateOrder:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getOrder:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)sendNotification:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSString * _Nullable responseData, NSError * _Nullable error))completion;

@end
