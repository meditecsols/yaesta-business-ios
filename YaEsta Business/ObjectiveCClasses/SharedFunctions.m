//
//  SharedFunctions.m

//
//  Created by Martin Gonzalez on 03/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "SharedFunctions.h"
#import "InvokeService.h"
#import "Config.h"

@interface SharedFunctions()
@end
@implementation SharedFunctions

+(SharedFunctions *)sharedInstance{
    
    static SharedFunctions *_sharedInstance =  nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[SharedFunctions alloc] init];
    });
    
    return _sharedInstance;
}

-(id)init{
    self = [super init];
    if(self){
        
    }
    return self;
}

-(UIViewController*)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"YaEstá!"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [[self topMostController] presentViewController:alert animated:NO completion:nil];
}





-(BOOL) isiPad{
    static BOOL isIPad = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isIPad = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad;
    });
    return isIPad;
}
-(NSString *)getCurrentStoryboard{
    NSString *nameStoryboard;
    if ([self isiPad]) {
        nameStoryboard =@"iPadStoryboard";
    }
    else{
        nameStoryboard = @"Main";
    }
    
    return nameStoryboard;
}

-(NSString *)formatDateToWS:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}
-(NSMutableDictionary *) dictionaryByReplacingNullsWithStrings : (NSMutableDictionary *) dictionary{
    const NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary: dictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in dictionary) {
        const id object = [dictionary objectForKey: key];
        if (object == nul) {
            [replaced setObject: blank forKey: key];
        }
        else if ([object isKindOfClass: [NSDictionary class]]) {
            [replaced setObject: [self dictionaryByReplacingNullsWithStrings:object] forKey: key];
        }else if ([object isKindOfClass: [NSArray class]]){
            [replaced setObject:[self arrayByReplacingNullsWithBlanks:object] forKey:key];
        }
    }
    return  replaced;
}

-(NSArray *)arrayByReplacingNullsWithBlanks  : (NSArray *) array{
    NSMutableArray *replaced = [array mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    for (int idx = 0; idx < [replaced count]; idx++) {
        id object = [replaced objectAtIndex:idx];
        if (object == nul) [replaced replaceObjectAtIndex:idx withObject:blank];
        else if ([object isKindOfClass:[NSDictionary class]])
            [replaced replaceObjectAtIndex:idx withObject:[self dictionaryByReplacingNullsWithStrings:object]];
        else if ([object isKindOfClass:[NSArray class]])
            [replaced replaceObjectAtIndex:idx withObject:[self arrayByReplacingNullsWithBlanks: object]];
    }
    return [replaced copy];
}


-(NSString *)getDateFormatToWS {
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter2 setLocale:enUSPOSIXLocale];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *dateFinal=[dateFormatter2 stringFromDate:[NSDate date]];
    return dateFinal;
}




    
    


@end
